.PHONY: help build start stop clean

PROJ_NAME_SHORT := "My Project"
BUILD_DATE := $(shell date +%s)

help: #Display this help message.
	@echo ""
	@echo "${PROJ_NAME_SHORT}, simple bookmark manager"
	@echo ""
	@grep '^[a-zA-Z_\-]\+:' Makefile | sed 's/:.*#[ ]*/:/' | column -s ':' -t | sort -h

build: #Create the X

start: #Start the X

stop: #Stop everything.

clean: stop #Stop everything, and remove X
